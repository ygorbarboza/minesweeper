package com.example.ygor.minesweeper.view;

import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.animation.AnimationUtils;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.ViewFlipper;

/**
 * Created by ygor on 7/3/2014.
 */
class CustomTableLayout extends TableLayout {

    private boolean mInterruptTouchEvent;

    public CustomTableLayout(Context context) {
        super(context);
    }

    public CustomTableLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public boolean isInterruptTouchEvent() {
        return mInterruptTouchEvent;
    }

    public void setInterruptTouchEvent(boolean interruptTouchEvent) {
        mInterruptTouchEvent = interruptTouchEvent;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        return mInterruptTouchEvent;
    }
}

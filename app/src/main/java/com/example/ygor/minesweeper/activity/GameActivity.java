package com.example.ygor.minesweeper.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import com.example.ygor.minesweeper.R;
import com.example.ygor.minesweeper.SaveGameManager;
import com.example.ygor.minesweeper.view.CellView;
import com.example.ygor.minesweeper.view.FieldView;

import java.util.List;
import java.util.Random;


public class GameActivity extends Activity implements FieldView.OnCellClickListener {

    private FieldView mFieldView;

    private int[] mMineCountValues;
    private int[] mTableSizeValues;

    private int mChosenMineCountValue;
    private int mChosenTableSizeValues;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        mFieldView = (FieldView) findViewById(R.id.field);

        mMineCountValues = getResources().getIntArray(R.array.num_mine_values);
        mTableSizeValues = getResources().getIntArray(R.array.table_size);

        mFieldView.setOnCellClickListener(this);

        newGame();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.game, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_difficult) {
            showMineCountDialog();
            return true;
        }
        if (id == R.id.action_table_size) {
            showTableSizeDialog();
            return true;
        }
        if (id == R.id.action_save_game) {
            saveGame();
            return true;
        }
        if (id == R.id.action_load_game) {
            loadGame();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onResetClick(View view) {
        newGame();
    }

    public void onCheatClick(View view) {
        mFieldView.cheat();
    }

    public void onValidateClick(View view) {
        validateRemainingCells();
    }

    private void showMineCountDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle(R.string.dialog_title_mine_count)
                .setSingleChoiceItems(R.array.dialog_itens_mine_count, mChosenMineCountValue, null)
                .setNegativeButton(R.string.cancel, null)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        ListView lw = ((AlertDialog) dialog).getListView();
                        mChosenMineCountValue = lw.getCheckedItemPosition();

                        newGame();
                    }
                });

        builder.show();
    }

    private void showTableSizeDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle(R.string.dialog_title_table_size)
                .setSingleChoiceItems(R.array.dialog_itens_table_size, mChosenTableSizeValues, null)
                .setNegativeButton(R.string.cancel, null)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        ListView lw = ((AlertDialog) dialog).getListView();
                        mChosenTableSizeValues = lw.getCheckedItemPosition();

                        newGame();
                    }
                });

        builder.show();
    }

    private void newGame() {
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                mFieldView.reset(mTableSizeValues[mChosenTableSizeValues]);
                addRandomMines(mMineCountValues[mChosenMineCountValue]);
            }
        });
    }

    private void addRandomMines(int numMine) {
        Random random = new Random();

        int count = 0;
        while (count < numMine) {
            int row = (random.nextInt(mTableSizeValues[mChosenTableSizeValues]));
            int column = (random.nextInt(mTableSizeValues[mChosenTableSizeValues]));

            if (mFieldView.addMine(row, column)) {
                count++;
            }
        }
    }

    private void endGame(int resId, Object... values) {
        mFieldView.setInterruptTouchEvent(true);
        showMessage(resId, values);
    }

    private void showMessage(int resId, Object... values) {
        Toast.makeText(this, getResources().getString(resId, values), Toast.LENGTH_SHORT).show();
    }

    public void validateRemainingCells() {
        int notCellNotRevealedCellCount = mFieldView.getNotMineNotRevealedCellCount();

        if (notCellNotRevealedCellCount == 0) {
            endGame(R.string.message_win);
        } else {
            endGame(R.string.message_validade_with_remaining_cells, notCellNotRevealedCellCount);
        }
    }

    public void saveGame() {
        SaveGameManager.getInstance(this).saveGame(
                mFieldView.getRevealedCellsPositionList(),
                mFieldView.getMineCellPositionList(),
                mFieldView.getFieldSize(),
                mFieldView.isInterruptTouchEvent());
        showMessage(R.string.message_game_saved);
    }

    public void loadGame() {
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                SaveGameManager savedGameManager = SaveGameManager.getInstance(GameActivity.this);

                if (!savedGameManager.hasSavedGame()) {
                    showMessage(R.string.message_no_saved_game);
                    return;
                }

                List<Integer> revealedCellsPositionList = savedGameManager.getRevealedCellsPositionList();
                List<Integer> minesPositionList = savedGameManager.getMinesPositionList();

                mFieldView.reset(savedGameManager.getFieldSize());

                for (int i = 0; i < minesPositionList.size(); i++) {
                    mFieldView.addMine(minesPositionList.get(i));
                }

                for (int i = 0; i < revealedCellsPositionList.size(); i++) {
                    mFieldView.revealIndividualCell(revealedCellsPositionList.get(i));
                }

                mFieldView.setInterruptTouchEvent(savedGameManager.isTouchEventIntercepted());
                showMessage(R.string.message_game_loaded);
            }
        });
    }

    @Override
    public void onCellClick(CellView cellView) {
        if(cellView.isMine()){
            endGame(R.string.message_lose);
        }
    }
}

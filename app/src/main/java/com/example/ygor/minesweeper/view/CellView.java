package com.example.ygor.minesweeper.view;

import android.content.Context;
import android.os.Handler;
import android.util.TypedValue;
import android.view.Gravity;
import android.widget.TextView;

import com.example.ygor.minesweeper.R;

/**
 * Created by ygor on 7/3/2014.
 */
public class CellView extends TextView {

    public static final int CHEAT_REVEAL_TIME_IN_MS = 1000;
    private static final int[] REVEALED_STATE = {R.attr.revealed};

    private int mMineAroundCount;
    private int mRow;
    private int mColumn;

    private boolean mIsMine;
    private boolean mIsRevealed;

    public CellView(Context context, int row, int column) {
        super(context);
        init(row, column);
    }

    private void init(int row, int column) {
        mRow = row;
        mColumn = column;

        setBackgroundResource(R.drawable.bg_cell_selector);
        setGravity(Gravity.CENTER);
        setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.cell_text_size));
    }

    public void reveal() {
        if (mIsMine) {
            setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_bomb, 0, 0, 0);
        } else {
            if (mMineAroundCount != 0) {
                setText(Integer.toString(mMineAroundCount));
            }
        }
        changeRevealState(true);
    }

    public void hide(int delay) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                hide();
            }
        }, delay);
    }

    public void hide() {
        changeRevealState(false);
        setText("");
        setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
    }

    private void changeRevealState(boolean isRevealed) {
        mIsRevealed = isRevealed;
        refreshDrawableState();
    }

    public void cheat(){
        if (!mIsRevealed) {
            reveal();
            hide(CHEAT_REVEAL_TIME_IN_MS);
        }
    }

    @Override
    protected int[] onCreateDrawableState(int extraSpace) {
        int[] state = super.onCreateDrawableState(extraSpace + 1);

        if (mIsRevealed) {
            mergeDrawableStates(state, REVEALED_STATE);
        }

        return state;
    }

    public int getMineAroundCount() {
        return mMineAroundCount;
    }

    public int getRow() {
        return mRow;
    }

    public int getColumn() {
        return mColumn;
    }

    public void incrementMineAroundCount() {
        mMineAroundCount++;
    }

    public boolean isMine() {
        return mIsMine;
    }

    public void setIsMine(boolean isMine) {
        mIsMine = isMine;
    }

    public boolean isRevealed() {
        return mIsRevealed;
    }

}

package com.example.ygor.minesweeper.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;

import com.example.ygor.minesweeper.R;

import java.util.ArrayList;

/**
 * Created by ygor on 7/3/2014.
 */
public class FieldView extends ScrollView implements View.OnClickListener {

    private final static TableLayout.LayoutParams TABLE_ROW_PARAMS = new TableLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

    private CellView mCells[][];
    private ArrayList<CellView> mMineCells;

    private int mFieldSize;
    private int mRevealedNonMineCellsCount;

    private CustomTableLayout mFieldTable;
    private TableRow.LayoutParams mCellParams;
    private OnCellClickListener mOnCellClickListener;

    public FieldView(Context context) {
        super(context);
        init();
    }

    public FieldView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        LayoutInflater.from(getContext()).inflate(R.layout.view_field, this, true);

        int cellSize = (int) getResources().getDimension(R.dimen.cell_size);

        mFieldTable = (CustomTableLayout) findViewById(R.id.field_table);
        mCellParams = new TableRow.LayoutParams(cellSize, cellSize);

        mCellParams.weight = 1;
        TABLE_ROW_PARAMS.weight = 1;
    }

    private void createCells() {
        mCells = new CellView[mFieldSize][mFieldSize];
        mMineCells = new ArrayList();
        mFieldTable.removeAllViews();

        for (int row = 0; row < mFieldSize; row++) {
            TableRow tableRow = new TableRow(getContext());

            for (int column = 0; column < mFieldSize; column++) {
                CellView cell = new CellView(getContext(), row, column);

                cell.setOnClickListener(FieldView.this);

                tableRow.addView(cell, mCellParams);
                mCells[row][column] = cell;
            }

            mFieldTable.addView(tableRow, TABLE_ROW_PARAMS);
        }
    }

    private void incrementCellValueAroundMine(int row, int column) {
        if (row > 0) {
            mCells[row - 1][column].incrementMineAroundCount();
            if (column < (mFieldSize - 1)) {
                mCells[row - 1][column + 1].incrementMineAroundCount();
            }
            if (column > 0) {
                mCells[row - 1][column - 1].incrementMineAroundCount();
            }
        }
        if (row < (mFieldSize - 1)) {
            mCells[row + 1][column].incrementMineAroundCount();
            if (column < (mFieldSize - 1)) {
                mCells[row + 1][column + 1].incrementMineAroundCount();
            }
            if (column > 0) {
                mCells[row + 1][column - 1].incrementMineAroundCount();
            }
        }
        if (column < (mFieldSize - 1)) {
            mCells[row][column + 1].incrementMineAroundCount();
        }
        if (column > 0) {
            mCells[row][column - 1].incrementMineAroundCount();
        }
    }

    @Override
    public void onClick(View view) {
        revealCell((CellView) view);
        if(mOnCellClickListener != null){
            mOnCellClickListener.onCellClick((CellView) view);
        }
    }

    public void revealIndividualCell(int position) {
        CellView cellView = getCellViewAtPosition(position);
        cellView.reveal();
        if(!cellView.isMine()){
            mRevealedNonMineCellsCount++;
        }
    }

    private void revealCell(CellView cell) {
        if (!cell.isRevealed()) {
            cell.reveal();

            if (!cell.isMine()) {
                mRevealedNonMineCellsCount++;
                if (cell.getMineAroundCount() == 0) {
                    revealCellsAround(cell);
                }
            }
        }
    }

    private void revealCellsAround(CellView cell) {
        int row = cell.getRow();
        int column = cell.getColumn();

        if (row > 0) {
            revealCell(mCells[row - 1][column]);

            if (column < (mFieldSize - 1)) {
                revealCell(mCells[row - 1][column + 1]);
            }
            if (column > 0) {
                revealCell(mCells[row - 1][column - 1]);
            }
        }
        if (row < (mFieldSize - 1)) {
            revealCell(mCells[row + 1][column]);

            if (column < (mFieldSize - 1)) {
                revealCell(mCells[row + 1][column + 1]);
            }
            if (column > 0) {
                revealCell(mCells[row + 1][column - 1]);
            }
        }
        if (column < (mFieldSize - 1)) {
            revealCell(mCells[row][column + 1]);
        }
        if (column > 0) {
            revealCell(mCells[row][column - 1]);
        }
    }

    public void reset(int fieldSize) {
        mFieldSize = fieldSize;
        mRevealedNonMineCellsCount = 0;
        mFieldTable.setInterruptTouchEvent(false);

        createCells();
    }

    public void cheat() {
        for (int i = 0; i < mMineCells.size(); i++) {
            mMineCells.get(i).cheat();
        }
    }

    public boolean addMine(int row, int column) {
        return addMine(mCells[row][column]);
    }

    public boolean addMine(int position) {
        return addMine(getCellViewAtPosition(position));
    }

    private boolean addMine(CellView cellView){
        if (!cellView.isMine()) {
            cellView.setIsMine(true);
            mMineCells.add(cellView);

            incrementCellValueAroundMine(cellView.getRow(), cellView.getColumn());
            return true;
        }
        return false;
    }

    public int getCellViewPosition(CellView cellView) {
        return convertCoordinatesToPosition(cellView.getRow(), cellView.getColumn());
    }

    public int convertCoordinatesToPosition(int row, int column) {
        return row * mFieldSize + column;
    }

    public CellView getCellViewAtPosition(int position) {
        return mCells[position / mFieldSize][position % mFieldSize];
    }

    public ArrayList<Integer> getRevealedCellsPositionList() {
        ArrayList<Integer> positionList = new ArrayList<Integer>();

        for (int row = 0; row < mFieldSize; row++) {
            for (int column = 0; column < mFieldSize; column++) {
                if (mCells[row][column].isRevealed()) {
                    positionList.add(getCellViewPosition(mCells[row][column]));
                }
            }
        }

        return positionList;
    }

    public ArrayList<Integer> getMineCellPositionList() {
        ArrayList<Integer> positionList = new ArrayList<Integer>();

        for (int i = 0; i < mMineCells.size(); i++) {
            positionList.add(getCellViewPosition(mMineCells.get(i)));
        }

        return positionList;
    }

    public int getNotMineNotRevealedCellCount() {
        int nonMineCellCount = (mFieldSize * mFieldSize) - mMineCells.size();
        return nonMineCellCount - mRevealedNonMineCellsCount;
    }

    public int getFieldSize() {
        return mFieldSize;
    }

    public boolean isInterruptTouchEvent() {
        return mFieldTable.isInterruptTouchEvent();
    }

    public void setInterruptTouchEvent(boolean interruptTouchEvent) {
        mFieldTable.setInterruptTouchEvent(interruptTouchEvent);
    }

    public void setOnCellClickListener(OnCellClickListener onCellClickListener){
        mOnCellClickListener = onCellClickListener;
    }

    public interface OnCellClickListener{
        public void onCellClick(CellView cellView);
    }

}

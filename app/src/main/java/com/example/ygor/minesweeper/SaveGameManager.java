package com.example.ygor.minesweeper;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.ArrayList;
import java.util.List;

public class SaveGameManager {
 
    private static final String PREF_NAME = "SAVED_GAME";
    private static final String KEY_REVEALED_CELLS = "revealed cells";
    private static final String KEY_MINE_POSITION = "mine position";
    private static final String KEY_FIELD_SIZE = "field size";
    private static final String KEY_TOUCH_EVENT_INTERCEPTED = "touch event intercepted";
 
    private static SaveGameManager sInstance;
    private final SharedPreferences mPref;
 
    private SaveGameManager(Context context) {
        mPref = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
    }
 
    public static synchronized SaveGameManager getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new SaveGameManager(context);
        }
        return sInstance;
    }
 
    public void saveGame(ArrayList revealedCellList, ArrayList minePositionList, int fieldSize, boolean isTouchEventIntercepted) {
        mPref.edit()
                .putString(KEY_REVEALED_CELLS, ObjectSerializer.serialize(revealedCellList))
                .putString(KEY_MINE_POSITION, ObjectSerializer.serialize(minePositionList))
                .putInt(KEY_FIELD_SIZE, fieldSize)
                .putBoolean(KEY_TOUCH_EVENT_INTERCEPTED, isTouchEventIntercepted)
                .commit();
    }

    public List getRevealedCellsPositionList() {
        return (List) ObjectSerializer.deserialize(mPref.getString(KEY_REVEALED_CELLS, ""));
    }

    public List getMinesPositionList() {
        return (List) ObjectSerializer.deserialize(mPref.getString(KEY_MINE_POSITION, ""));
    }

    public int getFieldSize() {
        return mPref.getInt(KEY_FIELD_SIZE, 0);
    }

    public boolean isTouchEventIntercepted() {
        return mPref.getBoolean(KEY_TOUCH_EVENT_INTERCEPTED, false);
    }

    public boolean hasSavedGame() {
        return mPref.contains(KEY_FIELD_SIZE);
    }
 
    public boolean clear() {
        return mPref.edit()
                .clear()
                .commit();
    }
}
package com.example.ygor.minesweeper;

import android.test.ActivityInstrumentationTestCase2;
import android.widget.TableLayout;
import android.widget.TableRow;

import com.example.ygor.minesweeper.activity.GameActivity;
import com.example.ygor.minesweeper.view.CellView;
import com.example.ygor.minesweeper.view.FieldView;

import java.util.ArrayList;

/**
 * <a href="http://d.android.com/tools/testing/testing_android.html">Testing Fundamentals</a>
 */
public class GameActivityTest extends ActivityInstrumentationTestCase2<GameActivity> {
    private CustomSolo mSolo;
    private int[] mTableSizes;
    private int[] mNumMineValues;

    public GameActivityTest() {
        super(GameActivity.class);
    }

    protected void setUp() throws Exception {
        super.setUp();

        mTableSizes = getActivity().getResources().getIntArray(R.array.table_size);
        mNumMineValues = getActivity().getResources().getIntArray(R.array.num_mine_values);
        mSolo = new CustomSolo(getInstrumentation(), getActivity());
    }

    @Override
    public void tearDown() throws Exception {
        mSolo.finishOpenedActivities();
    }

    public void test_change_table_size() {
        TableLayout tableLayout = (TableLayout) getActivity().findViewById(R.id.field_table);

        for (int i = 0; i < mTableSizes.length; i++) {
            mSolo.clickOnActionBarItem(R.id.action_table_size);
            mSolo.clickInList(i + 1);
            mSolo.clickOnText(R.string.ok);

            mSolo.sleep(2000);

            assertTrue(tableLayout.getChildCount() == mTableSizes[i]);
        }
    }

    public void test_mine_size() {
        for (int numMinePos = 0; numMinePos < mNumMineValues.length; numMinePos++) {
            mSolo.clickOnActionBarItem(R.id.action_difficult);
            mSolo.clickInList(numMinePos + 1);
            mSolo.clickOnText(R.string.ok);

            mSolo.sleep(2000);

            ArrayList<CellView> cellViews = mSolo.getCurrentViews(CellView.class);
            int mineCount = 0;

            for (int viewPos = 0; viewPos < cellViews.size(); viewPos++) {
                if (cellViews.get(viewPos).isMine()) {
                    mineCount++;
                }
            }

            assertTrue(mineCount == mNumMineValues[numMinePos]);
        }
    }

    public void test_cheat() {
        ArrayList<CellView> cellViews = mSolo.getCurrentViews(CellView.class);
        int revealedMineCount = 0;

        mSolo.clickOnText(R.string.button_cheat);

        mSolo.sleep(100);

        for (int viewPos = 0; viewPos < cellViews.size(); viewPos++) {

            if (cellViews.get(viewPos).isRevealed()) {
                revealedMineCount++;
            }

        }

        assertTrue(revealedMineCount == mNumMineValues[0]);
    }

    public void test_save_game_mine_position() {
        FieldView fieldView = mSolo.getCurrentViews(FieldView.class).get(0);
        ArrayList<CellView> cellViewsList = mSolo.getCurrentViews(CellView.class);
        ArrayList<Integer> minesPosition = new ArrayList<Integer>();

        for (int viewPos = 0; viewPos < cellViewsList.size(); viewPos++) {
            if (cellViewsList.get(viewPos).isMine()) {
                minesPosition.add(fieldView.getCellViewPosition(cellViewsList.get(viewPos)));
            }
        }

        mSolo.clickOnActionBarItem(R.id.action_save_game);
        assertTrue(mSolo.waitForText(R.string.message_game_saved));

        mSolo.clickOnActionBarItem(R.id.action_difficult);
        mSolo.clickInList(mNumMineValues.length);
        mSolo.clickOnText(R.string.ok);
        mSolo.sleep(1000);

        mSolo.clickOnActionBarItem(R.id.action_load_game);
        assertTrue(mSolo.waitForText(R.string.message_game_loaded));

        cellViewsList = mSolo.getCurrentViews(CellView.class);

        for (int viewPos = 0; viewPos < cellViewsList.size(); viewPos++) {
            if (cellViewsList.get(viewPos).isMine()) {
                assertTrue(minesPosition.contains(fieldView.getCellViewPosition(cellViewsList.get(viewPos))));
            }
        }

        assertTrue(minesPosition.size() == mNumMineValues[0]);
    }

    public void test_save_game_with_revealed_cell() {
        FieldView fieldView = mSolo.getCurrentViews(FieldView.class).get(0);
        ArrayList<CellView> cellViewsList = mSolo.getCurrentViews(CellView.class);
        ArrayList<Integer> revealedPosition = new ArrayList<Integer>();

        mSolo.clickOnView(cellViewsList.get(0));
        mSolo.sleep(100);

        for (int viewPos = 0; viewPos < cellViewsList.size(); viewPos++) {
            if (cellViewsList.get(viewPos).isRevealed()) {
                revealedPosition.add(fieldView.getCellViewPosition(cellViewsList.get(viewPos)));
            }
        }

        mSolo.clickOnActionBarItem(R.id.action_save_game);
        assertTrue(mSolo.waitForText(R.string.message_game_saved));

        mSolo.clickOnText(R.string.button_reset);
        mSolo.sleep(1000);

        mSolo.clickOnActionBarItem(R.id.action_load_game);
        assertTrue(mSolo.waitForText(R.string.message_game_loaded));

        cellViewsList = mSolo.getCurrentViews(CellView.class);

        for (int viewPos = 0; viewPos < cellViewsList.size(); viewPos++) {
            if (cellViewsList.get(viewPos).isRevealed()) {
                assertTrue(revealedPosition.contains(fieldView.getCellViewPosition(cellViewsList.get(viewPos))));
            }
        }
    }

    public void test_save_game_changed_table_size() {
        TableLayout tableLayout = (TableLayout) getActivity().findViewById(R.id.field_table);
        int rowCount;

        mSolo.clickOnActionBarItem(R.id.action_save_game);
        assertTrue(mSolo.waitForText(R.string.message_game_saved));

        rowCount = tableLayout.getChildCount();

        mSolo.clickOnActionBarItem(R.id.action_table_size);
        mSolo.clickInList(mTableSizes.length);
        mSolo.clickOnText(R.string.ok);
        mSolo.sleep(1000);

        mSolo.clickOnActionBarItem(R.id.action_load_game);
        assertTrue(mSolo.waitForText(R.string.message_game_loaded));

        assertTrue(rowCount == tableLayout.getChildCount());
    }

    public void test_save_game_changed_mine_count() {
        TableLayout tableLayout = (TableLayout) getActivity().findViewById(R.id.field_table);
        int rowCount;

        mSolo.clickOnActionBarItem(R.id.action_save_game);
        assertTrue(mSolo.waitForText(R.string.message_game_saved));

        rowCount = tableLayout.getChildCount();

        mSolo.clickOnActionBarItem(R.id.action_table_size);
        mSolo.clickInList(mTableSizes.length);
        mSolo.clickOnText(R.string.ok);
        mSolo.sleep(1000);

        mSolo.clickOnActionBarItem(R.id.action_load_game);
        assertTrue(mSolo.waitForText(R.string.message_game_loaded));

        assertTrue(rowCount == tableLayout.getChildCount());
    }

    public void test_no_saved_game() {
        SaveGameManager.getInstance(getInstrumentation().getTargetContext()).clear();

        mSolo.clickOnActionBarItem(R.id.action_load_game);
        assertTrue(mSolo.waitForText(R.string.message_no_saved_game));
    }

    public void test_lose() {
        ArrayList<CellView> cellViews = mSolo.getCurrentViews(CellView.class);

        for (int viewPos = 0; viewPos < cellViews.size(); viewPos++) {

            if (cellViews.get(viewPos).isMine()) {
                mSolo.clickOnView(cellViews.get(viewPos));
                break;
            }

        }

        assertTrue(mSolo.searchText(mSolo.getString(R.string.message_lose), false));
    }

    public void test_validate() {
        int nonMineCell = mTableSizes[0] * mTableSizes[0] - mNumMineValues[0];

        mSolo.clickOnText(R.string.button_validate);

        assertTrue(mSolo.waitForText(mSolo.getCurrentActivity().getString(R.string.message_validade_with_remaining_cells, nonMineCell)));
    }

    public void test_win() {
        clickAllNonMineCells();
        mSolo.clickOnText(R.string.button_validate);

        assertTrue(mSolo.searchText(mSolo.getString(R.string.message_win), false));
    }

    private void clickAllNonMineCells() {
        try {
            runTestOnUiThread(new Runnable() {
                                  @Override
                                  public void run() {
                                      TableLayout tableLayout = (TableLayout) getActivity().findViewById(R.id.field_table);

                                      for (int row = 0;
                                           row < tableLayout.getChildCount(); row++) {
                                          TableRow tableRow = (TableRow) tableLayout.getChildAt(row);

                                          for (int column = 0; column < tableRow.getChildCount(); column++) {
                                              CellView cell = (CellView) tableRow.getChildAt(column);

                                              if (!cell.isMine()) {
                                                  cell.performClick();
                                              }
                                          }
                                      }
                                  }
                              }
            );
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }

}
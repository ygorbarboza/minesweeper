package com.example.ygor.minesweeper;

import android.app.Activity;
import android.app.Instrumentation;

import com.robotium.solo.Solo;

/**
 * Created by ygor on 7/15/2014.
 */
public class CustomSolo extends Solo {
    public CustomSolo(Instrumentation instrumentation, Activity activity) {
        super(instrumentation, activity);
    }

    public CustomSolo(Instrumentation instrumentation, Config config) {
        super(instrumentation, config);
    }

    public CustomSolo(Instrumentation instrumentation, Config config, Activity activity) {
        super(instrumentation, config, activity);
    }

    public CustomSolo(Instrumentation instrumentation) {
        super(instrumentation);
    }

    public void clickOnText(int textId) {
        super.clickOnText(getString(textId));
    }

    public boolean waitForText(int textId) {
        return super.waitForText(getString(textId));
    }

}
